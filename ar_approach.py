import pandas as pd
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import apriori, association_rules
from tqdm import tqdm


def main():
    prescription = pd.read_csv('data/graph-20200715.csv', header=None)
    prescription.columns = ['prescription', 'drug']

    ta_df = nodes_to_transactions(prescription)

    frequent_itemsets = apriori(ta_df, min_support=0.001, use_colnames=True)
    rules = association_rules(frequent_itemsets, metric='confidence', min_threshold=0.7)
    rules['antecedent_len'] = rules['antecedents'].apply(lambda x: len(x))

    # find rules containing our drugs
    r = rules[rules['antecedents'] >= {624, 1144}]
    # rules with just our drugs
    er = rules[rules['antecedents'] == {624, 1144}]

    print('done')


def nodes_to_transactions(nodes: pd.DataFrame) -> pd.DataFrame:
    dataset = []
    prescription = nodes['prescription'].unique()
    for p in tqdm(prescription):
        tmp = nodes[nodes['prescription'] == p]
        dataset.append(list(tmp['drug'].values))

    te = TransactionEncoder()
    te_ary = te.fit(dataset).transform(dataset)
    df = pd.DataFrame(te_ary, columns=te.columns_)

    return df


if __name__ == '__main__':
    main()
