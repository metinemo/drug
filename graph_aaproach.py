import pandas as pd
import networkx as nx
import numpy as np
from sklearn.cluster import DBSCAN
import community


def main():
    claaified_nodes = pd.read_csv(r'data/groupby_drugs.csv')
    nodes = pd.read_csv('data/graph-20200715.csv', header=None)
    nodes.columns = ['node', 'drug']

    # nodes.dropna(inplace=True)
    # edges = nodes.merge(nodes, how='inner', on='drug')
    # edges = edges.groupby(['node_x', 'node_y'], as_index=False)['drug'].count()
    # edges.columns = ['source', 'target', 'weight']
    # edges.drop(edges.loc[edges['source'] > edges['target']].index.tolist(), inplace=True)

    nodes = nodes.groupby(['node'], as_index=False).count()

    edges = pd.read_csv('data/cooccurrence-20200715_v2.csv', header=None)
    edges.columns = ['source', 'target', 'weight']

    jac_edges = jaccard_sim(edges, nodes)
    data_sim, sim_df = sim_nodes_detector(jac_edges)
    # prune edges
    # data_sim = data_sim[data_sim['jaccard_sim'] >= prune_thresh]

    G = nx.from_pandas_edgelist(data_sim, source='source', target='target', edge_attr=True)

    partitions = clustering_graph(G.copy(), noise_deletion=True, eps=0.9, min_samples=5)

    sim_df = sim_df.merge(partitions, how='left', left_on='id', right_index=True)
    sim_df.drop(['id'], axis=1, inplace=True)
    partitions = partitions.append(sim_df)
    partitions.to_csv('data/partitions.csv')
    # nodes = nodes[~nodes['node'].isin(sim_list)]
    # jac_edges = jac_edges[(~jac_edges['source'].isin(sim_list)) & (~jac_edges['target'].isin(sim_list))]
    #
    # G = nx.from_pandas_edgelist(jac_edges, source='source', target='target', edge_attr=True)

    print('done')


def jaccard_sim(edges: pd.DataFrame, nodes: pd.DataFrame) -> pd.DataFrame:
    nodes.columns = ['node', 'node_weight']

    edges = edges.merge(nodes, how='left', left_on='source', right_on='node')
    edges = edges.merge(nodes, how='left', left_on='target', right_on='node')

    edges['jaccard_sim'] = edges['weight'] / (edges['node_weight_x'] + edges['node_weight_y'] - edges['weight'])
    edges.drop(edges.columns.difference(['source', 'target', 'jaccard_sim']), axis=1, inplace=True)
    # data['jaccard_sim'] = data['jaccard_sim'].round(4)

    return edges


def sim_nodes_detector(data_sim: pd.DataFrame) -> pd.DataFrame:
    sim_nodes = data_sim[(data_sim['jaccard_sim'] == 1) & (data_sim['source'] != data_sim['target'])]
    if sim_nodes.empty:
        return data_sim, pd.DataFrame()
    # sim_nodes = sim_nodes[sim_nodes['source'] != sim_nodes['target']]
    sim_nodes = sim_nodes.groupby('source')['target'].apply(list)

    sim_nodes = pd.DataFrame(sim_nodes)
    slist = []
    sim_dic = {}
    for index, row in sim_nodes.iterrows():
        if not index in slist:
            for s in row['target']:
                sim_dic.update({s: index})

        slist.extend(row['target'])

    data_sim = data_sim[(~data_sim['source'].isin(slist)) & (~data_sim['target'].isin(slist))]
    sim_df = pd.DataFrame.from_dict(sim_dic, orient='index')
    sim_df.columns = ['id']
    return data_sim, sim_df


def sim_messages(edges, nodes, sim_thresh):
    sim_nodes = edges[edges['jaccard_sim'] >= sim_thresh]
    if sim_nodes.empty:
        return edges, pd.DataFrame()

    G = nx.from_pandas_edgelist(sim_nodes, source='source', target='target', edge_attr=True)
    node_dic = dict(zip(nodes['node'], nodes['node_weight']))
    nx.set_node_attributes(G, node_dic, 'label')
    cc = nx.connected_components(G)
    sim_list = []
    for c in cc:
        sub_nodes = nodes[nodes['node'].isin(c)]
        sub_nodes = sub_nodes.sort_values(['node_weight'], ascending=False)
        sim_list.extend(sub_nodes.iloc[1:]['node'])

    return sim_list


def clustering_graph(G: nx.Graph, noise_deletion=True, eps=0.9, min_samples=5) -> pd.DataFrame:
    sim = adj_matrix(G)
    dis = 1 - sim

    if noise_deletion:
        dbs = DBSCAN(eps=eps, min_samples=min_samples, metric='precomputed').fit(dis)

        # Remove Noises from Graph
        noise_nodes = np.where(dbs.labels_ == -1)[0]
        noise_nodes = dis.index[noise_nodes]
        G.remove_nodes_from(noise_nodes)

    partitions = community.best_partition(G)

    if noise_deletion:
        noise_dic = {k: -1 for k in noise_nodes}
        partitions.update(noise_dic)

    partitions = pd.DataFrame.from_dict(partitions, orient='index')
    partitions.columns = ['class']

    return partitions


def adj_matrix(G: nx.Graph, weight='weight'):
    all_nodes = list(G.nodes)
    sim = nx.to_numpy_array(G, weight=weight)

    sim = pd.DataFrame(sim)
    sim.index = all_nodes
    sim.columns = all_nodes

    return sim


if __name__ == '__main__':
    main()
