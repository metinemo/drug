import json
import pandas as pd

def main():
    with open('hixTenThousandExport/hix_tenThousandsRecord.json', 'r', encoding='utf-8')as f:
        data = json.load(f)

    df = data['hits']['hits']
    df = pd.DataFrame(df)
    print('done')


if __name__ == '__main__':
    main()
