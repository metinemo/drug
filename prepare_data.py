import numpy as np
import pandas as pd
import jenkspy
import json
import networkx as nx
import graph_aaproach as ga
from tqdm import tqdm
import os
from os.path import join


def json_to_series(text):
    return [int(item['acceptedMedicationItemInfo']['genericCode']['content']) for item in
            json.loads(text)['medicationsInfo']]


def drug_tuple(text):
    return [(int(item['acceptedMedicationItemInfo']['genericCode']['content']),
             item['acceptedMedicationItemInfo']['genericCode']['meaning']) for item in
            json.loads(text)['medicationsInfo']]


def drug_dic(data):
    result = (data['data'].apply(drug_tuple)).to_frame()

    drug_dictionary = {}
    for index, row in tqdm(result.iterrows(), total=result.shape[0]):
        for item in row['data']:
            if item[0] not in drug_dictionary:
                drug_dictionary.update({item[0]: item[1]})

    drug_df = pd.DataFrame.from_dict(drug_dictionary, orient='index')
    drug_df.reset_index(inplace=True)
    drug_df.columns = ['drug_id', 'meaning']
    # drug_df.to_excel('data/drug_dictionary.xlsx')
    return drug_df


def noise_drugs(data):
    data_gr = data.groupby(by=['drug'], as_index=False)['id'].count()

    n_clusters = 5
    class_labels = []
    for i in range(n_clusters):
        class_labels.append(i)

    breaks = jenkspy.jenks_breaks(data_gr['id'], nb_class=n_clusters)
    data_gr['cut_jenks'] = pd.cut(data_gr['id'], bins=breaks, labels=class_labels, include_lowest=True)
    data_gr.columns = ['drug_id', 'count', 'cut_jenks']
    clusters = data_gr.groupby(['cut_jenks'], as_index=False).agg({'count': ['min', 'max', 'count']})
    # noise = data_gr[(data_gr['cut_jenks'] != 4) & (data_gr['cut_jenks'] != 3)]

    return data_gr, clusters


def prepare_db():
    data = pd.read_csv('data/prescriptions-20200814/prescriptions.csv')

    drug_df = drug_dic(data)

    result = (data['data'].apply(json_to_series)).to_frame()
    result.reset_index(inplace=True)
    result.rename(columns={'index': 'id'}, inplace=True)
    alldr = pd.DataFrame({'id': np.repeat(result['id'].values, result['data'].str.len()),
                          'drug': np.concatenate(result['data'].values)})
    alldr['count'] = 1
    alldr = alldr.groupby(['id', 'drug'], as_index=False).sum()

    data_gr, clusters = noise_drugs(alldr)

    data_gr = pd.merge(data_gr, drug_df, how='left', on='drug_id')
    data_gr.to_csv('data/drug_clustered_based_on_prescriptions_count.csv', index=False)
    clusters.to_csv('data/drug_clusters_jenks.csv', index=False)

    not_noise = data_gr[(data_gr['cut_jenks'] != 4) & (data_gr['cut_jenks'] != 3)]
    alldr = alldr[alldr['drug'].isin(not_noise['drug_id'].values)]

    nodes = alldr.groupby(['id'], as_index=False)['drug'].count()
    nodes.columns = ['id', 'weight']

    # remove words in just one message
    keywords_dic = alldr.groupby(['drug'], as_index=False)['id'].count()
    keywords_dic = keywords_dic[keywords_dic['id'] > 1]

    # create keywords dictionary
    keywords_dic.reset_index(drop=True, inplace=True)
    keywords_dic.reset_index(inplace=True)
    keywords_dic['index'] = keywords_dic['index'].astype(int)
    keywords_dic.drop(['id'], axis=1, inplace=True)

    alldr = alldr[alldr['drug'].isin(keywords_dic['drug'])]
    alldr = alldr.merge(keywords_dic, how='left', left_on='drug', right_on='drug')
    alldr.drop(alldr.columns.difference(['id', 'index']), axis=1, inplace=True)

    edges = alldr.merge(alldr, how='inner', on='index')
    edges = edges.groupby(['id_x', 'id_y'], as_index=False)['index'].count()

    edges.columns = ['source', 'target', 'weight']
    # undirected graph & remove selfloops
    edges = edges[edges['source'] < edges['target']]

    edges.reset_index(drop=True, inplace=True)

    return edges, nodes, result

    print('done')


def prepare_db_drug_as_nodes():
    data = pd.read_csv('data/prescriptions-20200814/prescriptions.csv')

    drug_df = drug_dic(data)

    result = (data['data'].apply(json_to_series)).to_frame()
    result.reset_index(inplace=True)
    result.rename(columns={'index': 'id'}, inplace=True)
    alldr = pd.DataFrame({'id': np.repeat(result['id'].values, result['data'].str.len()),
                          'drug': np.concatenate(result['data'].values)})
    alldr['count'] = 1
    alldr = alldr.groupby(['id', 'drug'], as_index=False).sum()

    data_gr, clusters = noise_drugs(alldr)

    data_gr = pd.merge(data_gr, drug_df, how='left', on='drug_id')
    data_gr.to_csv('data/drug_clustered_based_on_prescriptions_count.csv', index=False)
    clusters.to_csv('data/drug_clusters_jenks.csv', index=False)

    not_noise = data_gr[(data_gr['cut_jenks'] != 4) & (data_gr['cut_jenks'] != 3)]
    alldr = alldr[alldr['drug'].isin(not_noise['drug_id'].values)]

    # nodes = alldr.groupby(['drug'], as_index=False)['id'].count()
    # nodes.columns = ['id', 'weight']

    # remove words in just one message
    keywords_dic = alldr.groupby(['id'], as_index=False)['drug'].count()
    keywords_dic = keywords_dic[keywords_dic['drug'] > 1]

    # create keywords dictionary
    keywords_dic.reset_index(drop=True, inplace=True)
    keywords_dic.reset_index(inplace=True)
    keywords_dic['index'] = keywords_dic['index'].astype(int)
    keywords_dic.drop(['drug'], axis=1, inplace=True)

    alldr = alldr[alldr['id'].isin(keywords_dic['id'])]
    alldr = alldr.merge(keywords_dic, how='left', left_on='id', right_on='id')
    alldr.drop(alldr.columns.difference(['drug', 'index']), axis=1, inplace=True)

    edges = alldr.merge(alldr, how='inner', on='index')
    edges = edges.groupby(['drug_x', 'drug_y'], as_index=False)['index'].count()

    edges.columns = ['source', 'target', 'weight']
    nodes = edges[edges['source'] == edges['target']].copy()
    nodes.drop(['target'], axis=1, inplace=True)
    nodes.columns = ['id', 'weight']
    nodes = nodes.merge(drug_df, how='left', left_on='id', right_on='drug_id')
    # undirected graph & remove selfloops
    edges = edges[edges['source'] < edges['target']]

    edges.reset_index(drop=True, inplace=True)

    return edges, nodes, result

    print('done')


def meaning_to_atc():
    data = pd.read_csv('data/prescriptions-20200814/prescriptions.csv')

    drug_df = drug_dic(data)
    drug_df['meaning'] = drug_df['meaning'].str.lower()

    path = 'data/ATC dataset'
    drugs_atc = pd.DataFrame()
    for f in os.listdir(path):
        r = 'res_' + f
        f = join(path, f)
        # r = join(path, r)
        tmp = pd.read_excel(f)
        tmp = tmp.iloc[:, 0:3]
        tmp.columns = ['id', 'ATC_Code', 'Medicine']
        drugs_atc = drugs_atc.append(tmp)

    drugs_atc.dropna(inplace=True)
    drugs_atc.sort_values(by=['id'], inplace=True)
    drugs_atc['Medicine'] = drugs_atc['Medicine'].str.lstrip()
    drugs_atc['Medicine'] = drugs_atc['Medicine'].str.lower()
    drugs_atc['ATC_Code'] = drugs_atc['ATC_Code'].str.lstrip()
    drugs_atc.reset_index(drop=True, inplace=True)
    drugs_atc.reset_index(inplace=True)
    drugs_atc.rename(columns={'index': 'atc_id'}, inplace=True)

    meaning_expand = drug_df['meaning'].str.split(expand=True)
    medicine_expand = drugs_atc['Medicine'].str.split(expand=True)

    meaning_expand = pd.merge(meaning_expand, drug_df, left_index=True, right_index=True)
    medicine_expand = pd.merge(medicine_expand, drugs_atc['atc_id'], left_index=True, right_index=True)

    meaning_expand = meaning_expand.merge(medicine_expand[[0, 'atc_id']], how='left', on=0)
    meaning_expand.dropna(subset=['atc_id'], inplace=True)
    meaning_expand = meaning_expand.merge(drugs_atc, how='left', on='atc_id')

    meaning_group = meaning_expand.groupby(['drug_id'])['atc_id'].count()
    meaning_group_one = meaning_expand[meaning_expand['drug_id'].isin(meaning_group[meaning_group == 1].index)][
        ['drug_id', 'meaning', 'atc_id', 'ATC_Code', 'Medicine']]

    meaning_expand = meaning_expand[meaning_expand['drug_id'].isin(meaning_group[meaning_group > 1].index)]

    meaning_expand.dropna(subset=[1], inplace=True)
    medicine_expand.dropna(subset=[1], inplace=True)
    meaning_expand = meaning_expand.merge(medicine_expand[[0, 1, 'atc_id']], how='left', on=[0, 1])
    meaning_expand.dropna(subset=['atc_id'], inplace=True)
    meaning_expand = meaning_expand.merge(drugs_atc, how='left', on='atc_id')

    meaning_group = meaning_expand.groupby(['drug_id'])['atc_id'].count()
    meaning_group_one.append(meaning_expand[meaning_expand['drug_id'].isin(meaning_group[meaning_group == 1].index)][
                                 ['drug_id', 'meaning', 'atc_id', 'ATC_Code', 'Medicine']], ignore_index=True)

    meaning_expand.drop(meaning_expand.columns.difference(['drug_id', 'meaning', 'atc_id', 'ATC_Code', 'Medicine']),
                        axis=1, inplace=True)

    meaning_group = meaning_expand.groupby(['drug_id'])['atc_id'].count()
    meaning_group = meaning_group[meaning_group > 1]

    meaning_group_one = meaning_expand[~meaning_expand['drug_id'].isin(meaning_group.index)]

    meaning_expand.to_excel('data/meaning_atc.xlsx', index=False)
    print('done')


def main():
    # edges, nodes, result = prepare_db()

    edges, nodes, result = prepare_db_drug_as_nodes()
    nodes[['drug_id', 'meaning']].to_csv('data/drugs_meaning.csv', index=False)
    nodes.drop(nodes.columns.difference(['id', 'weight']), axis=1, inplace=True)

    jac_edges = ga.jaccard_sim(edges, nodes)
    data_sim, sim_df = ga.sim_nodes_detector(jac_edges)

    # prune edges
    # data_sim = data_sim[data_sim['jaccard_sim'] >= 0.8]

    if len(data_sim) < 1:
        return {'warning': 'empty Graph'}

    G = nx.from_pandas_edgelist(data_sim, source='source', target='target', edge_attr=True)

    partitions = ga.clustering_graph(G.copy(), noise_deletion=True, eps=0.9, min_samples=5)
    sim_df = sim_df.merge(partitions, how='left', left_on='id', right_index=True)
    sim_df.drop(['id'], axis=1, inplace=True)
    partitions = partitions.append(sim_df)
    partitions.reset_index(inplace=True)
    partitions.columns = ['id', 'class']
    # partitions = partitions.merge(result, how='left', on='id')
    partitions.to_csv('data/partitions_drugs.csv', index=False)
    jac_edges.to_csv('data/jac_drug_edges.csv', index=False)

    print('done')


if __name__ == '__main__':
    # data = pd.read_csv('data/partitions.csv')
    # t = data.groupby(['class'], as_index=False)['id'].count()
    # meaning_to_atc()
    main()
